import hexchat

__module_name__ = "blocknotice.py"
__module_author__ = "Mika Wu"
__module_version__ = "0.1.0.151007"
__module_description__ = "Redirects notices to server tab."

def redirect_cb(word, word_eol, userdata):
	"""Print user message with an abbreviated username
	if it exceeds the specified trim length.

	Args:
		word: Passed by hook. Contains user nick, message, and op status.
		word_eol: Passed by hook. Gives "word" contents from the provided
				  index and onward.
		userdata: Additional argument in hook can be passed through userdata.
				  Used here to better handle multiple types of message events.

	Returns:
		EAT_* constant: Controls how Hexchat procedes when callback returns.
		Here we will use "EAT_ALL" to soft "delete" the original message
		in its original context.

	See HEXCHAT documentation for futher information.
	"""
	if word[0] == "*status":
		schan = hexchat.find_context(channel='Rizon')
		schan.emit_print("Server Text", word[1])
		return hexchat.EAT_ALL


# Refer to Settings/Text Events for additional event names.
hexchat.hook_print("Notice", redirect_cb)
print(__module_name__, __module_version__, "has been loaded.")
